<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .informativo{
                color:rgb(245, 66, 66);

                margin-top: 50px;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <div class="col-lg-12">
                <h1>END POINTS</h1>
            </div>
            <div class="col-lg-12">
                <h4>api_pesquisa - retorno json</h4><br>
                <span>Endpoint aceita parâmetros de pesquisa como (veiculo, marca, modelo, ano, preço, quilometragem, estado e origem). </span>
            </div>
            <div class="col-lg-12">
                <h4>api_detalhes - retorno json</h4><br>
                <span>Endpoint depende do id do anuncio para exibir detalhes do mesmo.</span>
            </div>
            <div class="col-lg-12 informativo">
                <span>* Sistema desenvolvido para fins de teste recrutamento (o sistema foi desenvolvido como conceito de api, trata-se de uma plataforma totalmente dependente da estrutura atual do site "seminovos bh")</span>
            </div>
        </div>

    </body>
</html>
