<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PesquisaController extends Controller
{

    public function index(Request $request){

    	$dados = [];
    	    	
    	//Monta a url
        $site = "https://seminovos.com.br";

        $site = $this->defineFiltros($request, $site);

       	$site = $site.'?ordenarPor=2&registrosPagina=-1';

        $veiculos = file_get_contents($site);
        
        $veiculos = explode('<div class="d-none schema-items">', $veiculos);

        foreach ($veiculos as $i => $veiculo) {
        	if ($i > 0) {
        		array_push($dados, $this->dadosVeiculo($veiculo));
        	}
        }

        return response()->json($dados);

    }

    private function defineFiltros($request, $site){

    	$anoInicial = "";
    	$anoFinal = "";

    	$precoInicial = "";
    	$precoFinal = "";

    	$kmInicial = "";
    	$kmFinal = "";

    	if (isset($request->veiculo)) $site .= "/".$request->veiculo;
    	if (isset($request->marca)) $site .= "/".$request->marca;
    	if (isset($request->modelo)) $site .= "/".$request->modelo;
    	if (isset($request->ano_inicial)) $anoInicial = $request->ano_inicial;
    	if (isset($request->ano_final)) $anofinal = $request->ano_final;

    	$site .= '/ano-'.$anoInicial."-".$anoFinal;

    	if (isset($request->preco_inicial)) $precoInicial = $request->preco_inicial;
    	if (isset($request->preco_final)) $precoFinal = $request->preco_final;

    	$site .= '/preco-'.$precoInicial."-".$precoFinal;

    	if (isset($request->km_inicial)) $kmInicial = $request->km_inicial;
    	if (isset($request->km_final)) $kmFinal = $request->km_final;

    	$site .= '/km-'.$kmInicial."-".$kmFinal;

    	if (isset($request->condicao)) $site .= '/estado-'.$request->condicao;
    	if (isset($request->origem)) $site .= '/origem-'.$request->origem;

    	return $site;

    }

    private function dadosVeiculo($veiculos){

    	$retorno = [];

    	$veiculos = explode('itemprop', $veiculos);

        $id = explode('content="', $veiculos[self::ID]);
        $id = str_replace('<span', '', $id[1]);

    	$retorno = [
            'id' => trim(str_replace("\">\n", "", $id)),
    		'tipo' => $this->extrairPropriedades($veiculos[self::TIPO]),
    		'marca' => $this->extrairPropriedades($veiculos[self::MARCA]),
    		'modelo' => $this->extrairPropriedades($veiculos[self::MODELO]),	
    		'titulo_anuncio' => $this->extrairPropriedades($veiculos[self::TITULO_ANUNCIO]),	
    		'descricao' => $this->extrairPropriedades($veiculos[self::DESCRICAO]),	
    		'preco' => $this->extrairPropriedades($veiculos[self::PRECO])    	
    	];


    	return $retorno;
    } 

    private function extrairPropriedades($veiculo){

    	$propriedade = strip_tags($veiculo, 'span');
    	$propriedade = explode('>', $propriedade);
    	
    	return str_replace("\n", "", $propriedade[1]);

    }

}
