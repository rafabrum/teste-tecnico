<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    CONST ID = 1;
	CONST TIPO = 4; 
	CONST MARCA = 5; 
	CONST MODELO = 6; 
	CONST TITULO_ANUNCIO = 7;
	CONST DESCRICAO = 8;
	CONST PRECO = 11;

	CONST ANO_DETALHES = 27;
	CONST TIPO_DETALHES = 10;
	CONST QUILOMETRAGEM_DETALHES = 28; 
	CONST MARCA_DETALHES = 11; 
	CONST MODELO_DETALHES = 12; 
	CONST OBSERVACAO_DETALHES = 31;
	CONST PRECO_DETALHES = 9;
	CONST CAMBIO_DETALHES = 28;
	CONST PORTAS_DETALHES = 28; 
	CONST COMBUSTIVEL_DETALHES = 29; 
	CONST COR_DETALHES = 30; 
	CONST PLACA_DETALHES = 7;
	CONST TROCA_DETALHES = 30;
	CONST OBS_DETALHES = 31;


}
