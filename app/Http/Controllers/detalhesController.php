<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class detalhesController extends Controller
{
    public function show($id){

    	$dados = [];
    	    	
    	//Monta a url
        $site = "https://seminovos.com.br/".$id;

        $atributos = file_get_contents($site);

        $atributos = explode('itemprop', $atributos);

        array_push($dados, $this->dadosVeiculo($atributos));


        return response()->json($dados);

    }

    private function dadosVeiculo($atributo){

    	$retorno = [];

    	$arrayAcessorios = [];

    	$ano = strip_tags($atributo[self::ANO_DETALHES], 'span'); 		
    	$ano = str_replace('Quilometragem', '', explode('>', $ano));
    	$ano = trim($ano[1]);

    	$tipo = strip_tags($atributo[self::TIPO_DETALHES], 'span');
    	$tipo = explode('>', $tipo);
    	$tipo = trim($tipo[1]);

    	$km = strip_tags($atributo[self::QUILOMETRAGEM_DETALHES], 'span');
    	$km = explode('>', $km);
    	$aux = explode(' ', $km[1]);
    	$km = $aux[0];
    	$cambio = $aux[2];

    	$marca = strip_tags($atributo[self::MARCA_DETALHES], 'span');
    	$marca = explode('>', $marca);
    	$marca = trim($marca[1]);

    	$modelo = strip_tags($atributo[self::MODELO_DETALHES], 'span');
    	$modelo = explode('>', $modelo);
    	$modelo = trim(substr($modelo[1], 0, 20));

    	$observacao = $atributo[self::OBSERVACAO_DETALHES];
    	$observacao = explode('<p class="description-print">', $observacao);
    	$observacao = explode('</p>', $observacao[1]);
    	$observacao = $observacao[0];
    	
    	$acessorios = $atributo[self::OBSERVACAO_DETALHES];
    	$acessorios = explode('<li class="col-print-3">', $acessorios);

    	$countArray = count($acessorios);

    	foreach ($acessorios as $key => $value) {
    		if ($key > 0 && ($key < ($countArray - 1))) {

    			$value = str_replace('<span class="description-print">', '', $value);
    			$value = str_replace('</span>', '', $value);
    			$value = str_replace('</li>', '', $value);
    			$value = str_replace("</ul><ul class='list-styled'>", "", $value);
    			array_push($arrayAcessorios, $value);
    		}
    	}

    	return [
    		'ano' => str_replace("\n", "", $ano), 
    		'tipo' => str_replace("\n", "", $tipo), 
    		'km' => str_replace("\n", "", $km), 
    		'detalhes' => str_replace("\n", "", $aux),
    		'marca' => str_replace("\n", "", $marca), 
    		'modelo' => str_replace("\n", "",$modelo), 
    		'observacao' => str_replace("\n", "", $observacao),
    		'acessorios' => str_replace("\n", "", $arrayAcessorios)
    	];
    } 




}
